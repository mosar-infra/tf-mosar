output "loadbalancing" {
  value     = module.loadbalancing
  sensitive = true
}

output "networking" {
  value = module.networking
}

output "IAM" {
  value = module.iam
}

output "secrets" {
  value = module.secrets
}

output "computing" {
  value = module.computing
}
