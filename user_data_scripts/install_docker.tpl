#!/bin/bash -xe
echo 'begin install ======================================================================='
until ping -c1 www.google.com &>/dev/null; do
    echo "Waiting for network ..."
    sleep 1
done
yum update -y
yum install -y jq
yum install -y wget
yum install -y docker
service docker start
systemctl enable --now docker
usermod -a -G docker ec2-user
curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
echo SPRING_DATASOURCE_USERNAME=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id flashcards-db --query SecretString --output text | jq -r .USERNAME) | tee -a /home/ec2-user/env_vars
echo SPRING_DATASOURCE_PASSWORD=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id flashcards-db --query SecretString --output text | jq -r .PASSWORD) | tee -a /home/ec2-user/env_vars
echo MYSQL_ROOT_PASSWORD=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id flashcards-db --query SecretString --output text | jq -r .ROOT_PASSWORD) | tee -a /home/ec2-user/env_vars
echo MYSQL_PASSWORD=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id flashcards-db --query SecretString --output text | jq -r .PASSWORD) | tee -a /home/ec2-user/env_vars
echo MYSQL_USER=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id flashcards-db --query SecretString --output text | jq -r .USERNAME) | tee -a /home/ec2-user/env_vars
echo MYSQL_DATABASE=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id flashcards-db --query SecretString --output text | jq -r .DATABASE) | tee -a /home/ec2-user/env_vars
